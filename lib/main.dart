import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tomisha/_widgets/sine_cosine_wave_clipper.dart';
import 'package:tomisha/_widgets/sliding_control.dart';
import 'package:tomisha/utils/app_assets.dart';
import 'package:tomisha/utils/app_strings.dart';
import 'package:tomisha/utils/app_style.dart';

import '_widgets/elevated_gradient_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tomisha',
      theme: ThemeData(
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int segmentedControlGroupValue = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _appBar(),
      body: Column(
        children: [
          Expanded(
            flex: 484,
            child: _homeBody(context),
          ),
          Expanded(
            flex: 120,
            child: _bottomBar(),
          ),
        ],
      ),
    );
  }

  Widget _slidingSegmentedControllerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, bottom: 30, left: 8, right: 8.0),
      child: SlidingSegmentedControl(
        segmentedControl: segmentedControlGroupValue,
        tabs: {
          0: sliderThumb(
              title: AppStrings.TABBAR_VALUE_ONE,
              isSelected: segmentedControlGroupValue < 1),
          1: sliderThumb(
              title: AppStrings.TABBAR_VALUE_TWO,
              isSelected: segmentedControlGroupValue == 1),
          2: sliderThumb(
              title: AppStrings.TABBAR_VALUE_THREE,
              isSelected: segmentedControlGroupValue == 2),
        },
        onValueChanged: (value) {
          if (value != null) {
            segmentedControlGroupValue = value;
            _changeWidgetState();
          }
        },
      ),
    );
  }

  void _changeWidgetState() {
    setState(() {});
  }

  Container sliderThumb({required String title, required bool isSelected}) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width * 2.26,
      height: 40,
      child: Text(title,
          style: TextStyle(
            color: isSelected ? Colors.white : AppStyle.tabbarTextColor,
          )),
    );
  }

  Widget _homeBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _homeTopPartWidget(context),
          _slidingSegmentedControllerWidget(context),
          _subHeadingTextWidget(),
          const SizedBox(
            height: 20,
          ),
          _imageOneWidget(),
          _imageTwoWidget(),
          _imageThreeWidget(),
        ],
      ),
    );
  }

  Widget _subHeadingTextWidget() {
    return AutoSizeText(
      getText(segmentedControlGroupValue),
      minFontSize: 21,
      maxFontSize: 25,
      maxLines: 2,
      textAlign: TextAlign.center,
      style: GoogleFonts.lato(
        color: AppStyle.headingOneColor,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Container _homeTopPartWidget(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.7,
      decoration: const BoxDecoration(
        gradient: AppStyle.backgroundGradient,
      ),
      child: Column(
        children: [
          const Spacer(
            flex: 18,
          ),
          Expanded(
            flex: 113,
            child: AutoSizeText(
              AppStrings.HOME_TEXT,
              minFontSize: 42,
              maxFontSize: 50,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                color: Colors.black,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const Spacer(
            flex: 18,
          ),
          Expanded(
            flex: 400,
            child: Image.asset(
              AppAssets.UNDRAW_AGREEMENT,
            ),
          )
        ],
      ),
    );
  }

  Widget _bottomBar() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 3,
            offset: const Offset(0, -1),
            color: AppStyle.shadowColor,
          )
        ],
        border: Border.all(
          color: AppStyle.borderColor,
          width: 1,
        ),
      ),
      child: SafeArea(
        child: Column(
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(
                  flex: 20,
                ),
                Expanded(
                  flex: 320,
                  child: ElevatedGradientButton(
                    gradient: AppStyle.appBarGradient,
                    child: AutoSizeText(
                      AppStrings.RAISED_BUTTON_TEXT,
                      minFontSize: 14,
                      maxFontSize: 17,
                      style: GoogleFonts.lato(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    onPressed: () {},
                  ),
                ),
                const Spacer(
                  flex: 20,
                ),
              ],
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }

  PreferredSize _appBar() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(67),
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(12),
            ),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                spreadRadius: 3,
                blurRadius: 6,
                offset: Offset(0, 3),
                color: AppStyle.shadowColor,
              ),
            ],
          ),
          child: Column(
            children: [
              Expanded(
                flex: 5,
                child: Container(
                  width: double.infinity,
                  decoration:
                      const BoxDecoration(gradient: AppStyle.appBarGradient),
                ),
              ),
              Expanded(
                flex: 62,
                child: Row(
                  children: [
                    const Spacer(flex: 19),
                    TextButton(
                      onPressed: () {},
                      child: AutoSizeText(
                        AppStrings.LOGIN,
                        minFontSize: 14,
                        maxFontSize: 17,
                        style: GoogleFonts.lato(
                          color: AppStyle.xGreen,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const Spacer(
                      flex: 1,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _imageOneWidget() {
    return Container(
      height: MediaQuery.of(context).size.height * (254 / 659),
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            flex: 110,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                AutoSizeText(
                  '1.',
                  minFontSize: 130,
                  maxFontSize: 156,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                    color: AppStyle.numberColor,
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 220,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    flex: 144, child: Image.asset(AppAssets.HOME_IMAGE_ONE)),
                const Spacer(),
                Expanded(
                  flex: 103,
                  child: Container(
                    padding: const EdgeInsets.only(right: 57.0),
                    alignment: Alignment.center,
                    child: AutoSizeText(
                      getNumberOneText(segmentedControlGroupValue),
                      minFontSize: 16,
                      maxFontSize: 19,
                      maxLines: 2,
                      textAlign: TextAlign.left,
                      style: GoogleFonts.lato(
                        color: AppStyle.numberColor,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _imageTwoWidget() {
    return ClipPath(
      clipper: SinCosineWaveClipper(
        verticalPosition: VerticalPosition.top,
        horizontalPosition: HorizontalPosition.right,
      ),
      child: ClipPath(
        clipper: SinCosineWaveClipper(),
        child: Container(
          height: 390,
          width: double.infinity,
          padding: const EdgeInsets.all(8.0),
          decoration: const BoxDecoration(
            gradient: AppStyle.backgroundGradient,
          ),
          child: Column(
            children: [
              Expanded(
                flex: 130,
                child: Row(children: [
                  Expanded(
                    flex: 106,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: AutoSizeText(
                        '2.',
                        minFontSize: 130,
                        maxFontSize: 156,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                          color: AppStyle.numberColor,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 205,
                    child: Container(
                      padding: const EdgeInsets.only(right: 57.0),
                      alignment: Alignment.bottomCenter,
                      child: AutoSizeText(
                        getNumberTwoText(segmentedControlGroupValue),
                        minFontSize: 16,
                        maxFontSize: 19,
                        maxLines: 2,
                        textAlign: TextAlign.left,
                        style: GoogleFonts.lato(
                          color: AppStyle.numberColor,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
              Expanded(
                flex: 162,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 24.0),
                  child:
                      Image.asset(getTwoImagePath(segmentedControlGroupValue)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _imageThreeWidget() {
    return Container(
      height: 360,
      child: Column(
        children: [
          Expanded(
            flex: 130,
            child: Row(children: [
              Expanded(
                flex: 106,
                child: Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: AutoSizeText(
                    '3.',
                    minFontSize: 130,
                    maxFontSize: 156,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(
                      color: AppStyle.numberColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 205,
                child: Container(
                  padding: const EdgeInsets.only(right: 57.0),
                  alignment: Alignment.bottomCenter,
                  child: AutoSizeText(
                    getNumberThreeText(segmentedControlGroupValue),
                    minFontSize: 16,
                    maxFontSize: 19,
                    maxLines: 2,
                    textAlign: TextAlign.left,
                    style: GoogleFonts.lato(
                      color: AppStyle.numberColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ]),
          ),
          Expanded(
            flex: 162,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 24.0),
              child: Image.asset(getThreeImagePath(segmentedControlGroupValue)),
            ),
          )
        ],
      ),
    );
  }
}

String getText(int tabValue) {
  switch (tabValue) {
    case 0:
      return AppStrings.HOME_TEXT_TWO;
    case 1:
      return AppStrings.HOME_TEXT_THREE;
    case 2:
      return AppStrings.HOME_TEXT_FOUR;

    default:
      return AppStrings.HOME_TEXT_FOUR;
  }
}

String getNumberOneText(int value) {
  switch (value) {
    case 0:
      return AppStrings.NUMBER_ONE_TEXT;
    case 1:
      return AppStrings.NUMBER_ONE_TEXT_TWO;
    case 2:
      return AppStrings.NUMBER_ONE_TEXT_THREE;

    default:
      return AppStrings.NUMBER_ONE_TEXT;
  }
}

String getNumberTwoText(int value) {
  switch (value) {
    case 0:
      return AppStrings.NUMBER_TWO_TEXT_ONE;
    case 1:
      return AppStrings.NUMBER_TWO_TEXT_TWO;
    case 2:
      return AppStrings.NUMBER_TWO_TEXT_THREE;

    default:
      return AppStrings.NUMBER_TWO_TEXT_ONE;
  }
}

String getTwoImagePath(int value) {
  switch (value) {
    case 0:
      return AppAssets.NUMBER_TWO_IMAGE_ONE;
    case 1:
      return AppAssets.NUMBER_TWO_IMAGE_TWO;
    case 2:
      return AppAssets.NUMBER_TWO_IMAGE_THREE;
    default:
      return AppAssets.NUMBER_TWO_IMAGE_ONE;
  }
}

String getThreeImagePath(int value) {
  switch (value) {
    case 0:
      return AppAssets.NUMBER_THREE_IMAGE_ONE;
    case 1:
      return AppAssets.NUMBER_THREE_IMAGE_TWO;
    case 2:
      return AppAssets.NUMBER_THREE_IMAGE_THREE;
    default:
      return AppAssets.NUMBER_THREE_IMAGE_ONE;
  }
}

String getNumberThreeText(int value) {
  switch (value) {
    case 0:
      return AppStrings.NUMBER_THREE_TEXT_ONE;
    case 1:
      return AppStrings.NUMBER_THREE_TEXT_TWO;
    case 2:
      return AppStrings.NUMBER_THREE_TEXT_THREE;
    default:
      return AppStrings.NUMBER_THREE_TEXT_ONE;
  }
}
