import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tomisha/_widgets/cupertino_setmented_control_custom.dart';
import 'package:tomisha/utils/app_style.dart';

class SlidingSegmentedControl extends StatelessWidget {
  final int segmentedControl;
  final Map<int, Widget> tabs;
  final Function(int?) onValueChanged;

  const SlidingSegmentedControl({
    Key? key,
    this.segmentedControl = 0,
    required this.tabs,
    required this.onValueChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoSlidingSegmentedControlCustom<int>(
        groupValue: segmentedControl,
        children: tabs,
        backgroundColor: Colors.white,
        thumbColor: AppStyle.selectedTabbarColor,
        onValueChanged: onValueChanged);
  }
}
