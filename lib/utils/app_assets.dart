class AppAssets {
  static const UNDRAW_AGREEMENT = 'assets/undraw_agreement_aajr.png';
  static const HOME_IMAGE_ONE = 'assets/undraw_Profile_data_re_v81r.png';
  static const HOME_IMAGE_THREE = 'assets/undraw_personal_file_222m.png';
  static const NUMBER_TWO_IMAGE_ONE = 'assets/undraw_task_31wc.png';
  static const NUMBER_TWO_IMAGE_TWO = 'assets/undraw_about_me_wa29.png';
  static const NUMBER_TWO_IMAGE_THREE = 'assets/undraw_job_offers_kw5d.png';
  static const NUMBER_THREE_IMAGE_ONE = 'assets/undraw_personal_file_222m.png';
  static const NUMBER_THREE_IMAGE_TWO =
      'assets/undraw_swipe_profiles1_i6mr.png';
  static const NUMBER_THREE_IMAGE_THREE =
      'assets/undraw_business_deal_cpi9.png';
}
