class AppStrings {
  static const String LOGIN = 'Login';
  static const String RAISED_BUTTON_TEXT = 'Kostenlos Registrieren';
  static const String HOME_TEXT = 'Deine Job website';
  static const String HOME_TEXT_TWO =
      'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter';
  static const String HOME_TEXT_THREE =
      'Drei einfache Schritte zu deinem neuen Mitarbeiter';
  static const String HOME_TEXT_FOUR =
      'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter';
  static const String NUMBER_ONE_TEXT = 'Erstellen dein Lebenslauf';
  static const String NUMBER_ONE_TEXT_TWO = 'Erstellen dein Unternehmensprofil';
  static const String NUMBER_ONE_TEXT_THREE =
      'Erstellen dein Unternehmensprofil';

  static const String NUMBER_TWO_TEXT_ONE = 'Erstellen dein Lebenslauf';

  static const String NUMBER_TWO_TEXT_TWO = 'Erstellen ein Jobinserat';
  static const String NUMBER_TWO_TEXT_THREE =
      'Erhalte Vermittlungs- angebot von Arbeitgeber';

  static const String NUMBER_THREE_TEXT_ONE = 'Mit nur einem Klick bewerben';
  static const String NUMBER_THREE_TEXT_TWO =
      'Wähle deinen neuen Mitarbeiter aus';
  static const String NUMBER_THREE_TEXT_THREE =
      'Vermittlung nach Provision oder Stundenlohn';
  static const String TABBAR_VALUE_ONE = 'Arbeitnehmer';
  static const String TABBAR_VALUE_TWO = 'Arbeitgeber';
  static const String TABBAR_VALUE_THREE = 'Temporärbüro';
}
