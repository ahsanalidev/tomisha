import 'dart:ui';

import 'package:flutter/material.dart';

class AppStyle {
  static const xGreen = Color(0xFF319795);
  static const xBlue = Color(0xFF3182CE);
  static const appBarGradient = LinearGradient(
    begin: Alignment(-1.0, 0.0),
    end: Alignment(0.0, 1.0),
    colors: [xGreen, xBlue],
  );
  static const xSkyBlue = Color(0xFFEBF4FF);
  static const xLightBlue = Color(0xFFE6FFFA);
  static const backgroundGradient = LinearGradient(
    begin: Alignment(-1.0, 1.0),
    end: Alignment(1.0, -1.0),
    colors: [xSkyBlue, xLightBlue],
  );
  static Color shadowColor = Colors.black.withAlpha(16);
  static Color borderColor = Color(0xFF707070);
  static Color textColor = Color(0xFF2D3748);
  static Color tabbarBorderColor = Color(0xFFCBD5E0);
  static Color selectedTabbarColor = Color(0xFF81E6D9);
  static Color tabbarTextColor = Color(0xFF319795);
  static Color textColorTwo = Color(0xFF4A5568);
  static Color numberColor = Color(0xFF718096);
  static Color headingOneColor = Color(0xFF4A5568);
}
